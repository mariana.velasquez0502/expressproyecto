import express from 'express';
import * as DigimonsController from './src/controllers/DigimonsController';
import * as PokemonController from './src/controllers/PokemonController';


export const router = express.Router();

router.get('/', (req, res) => {
    res.send('Hello World with Typescript!')
})

router.get('/ts', (req, res) => {
    res.send('Typescript es lo máximo!')
})

router.get('/digimons', DigimonsController.getAll);
router.get('/digimons/:id', DigimonsController.get);
router.get('/digimons/name/:name', DigimonsController.getByName);
router.get('/digimons/type/:type', DigimonsController.getByType);
router.get('/digimons/strong/:name1/:name2', DigimonsController.getStrong);
router.get('/digimons/weak/:name1/:name2', DigimonsController.getWeak);
router.get('/digimons/new/:id/:name/:typeName/:typeStrong/:typeWeak/:img', DigimonsController.newDigimon);

router.get('/pokemon', PokemonController.getAll);
router.get('/pokemon/:id', PokemonController.get);
router.get('/pokemon/name/:name', PokemonController.getByName);
router.get('/pokemon/type/:type', PokemonController.getByType);
router.get('/pokemon/strong/:name1/:name2', PokemonController.getStrong);
router.get('/pokemon/weak/:name1/:name2', PokemonController.getWeak);
router.get('/pokemon/new/:id/:name/:typeName/:typeStrong/:typeWeak/:img', PokemonController.newPokemon);

router.post("/", (req, res) => {
    console.log("Cuerpo:", req.body);   
    res.status(200).send(req.body);
}); 
