export interface MonsterTypeI{
    name: string
    strongAgainst: string
    weakAgainst: string
}
 