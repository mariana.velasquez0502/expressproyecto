import { Request, Response } from "express";
import PokemonService from "../services/PokemonService";

export function getAll(_: any, res: Response) {
    const pokemon = PokemonService.getAll();
    res.status(200).json(pokemon);
}

export function get(req: Request, res: Response) {
    try { 
        const id = req.params.id && +req.params.id || undefined;
        if(!id){ throw "Se requiere el ID del pokemon."}
        const pokemon = PokemonService.get(id);
        res.status(200).json(pokemon);
    } catch (error) {
        res.status(400).send(error);
    }
}

export function getByName(req: Request, res: Response) {
    try {
        const name = req.params.name && req.params.name || undefined;
        if(!name){ throw "Se requiere el name del pokemon."}
        const pokemon = PokemonService.getByName(name);
        res.status(200).json(pokemon);
    } catch (error) {
        res.status(400).send(error);
    }
}

export function getByType(req: Request, res: Response) {
    try {
        const type = req.params.type && req.params.type || undefined;
        if(!type){ throw "Se requiere el Tipo del pokemon."}
        const pokemons = PokemonService.getByType(type);
        res.status(200).json(pokemons);
    } catch (error) {
        res.status(400).send(error);
    }
}

export function getStrong(req: Request, res: Response) {
    try {
        const name1 = req.params.name1 && req.params.name1 || undefined;
        const name2 = req.params.name2 && req.params.name2 || undefined;
        if(!name1||!name2){ throw "Se requieren los dos Pokemones."}
        const isStrong = PokemonService.getStrong(name1,name2);
        res.status(200).json(isStrong);
    } catch (error) {
        res.status(400).send(error);
    }
}

export function getWeak(req: Request, res: Response) {
    try {
        const name1 = req.params.name1 && req.params.name1 || undefined;
        const name2 = req.params.name2 && req.params.name2 || undefined;
        if(!name1||!name2){ throw "Se requieren los dos Pokemones."}
        const isStrong = PokemonService.getWeak(name1,name2);
        res.status(200).json(isStrong);
    } catch (error) {
        res.status(400).send(error);
    } 
}

export function newPokemon(req: Request, res: Response) {
    try {
        const name = req.params.name && req.params.name || undefined;
        const id = parseInt(req.params.id && req.params.id) || undefined;
        const typeName = req.params.typeName && req.params.typeName || undefined;
        const typeStrong = req.params.typeStrong && req.params.typeStrong || undefined;
        const typeWeak = req.params.typeWeak && req.params.typeWeak || undefined;
        const img = req.params.img && req.params.img || undefined;

        if(!name||!id||!typeName||!typeStrong||!typeWeak||!img){ throw "Se requieren todos los datos del Pokemon."}
        const pokemon = PokemonService.newPokemon(id,name,typeName,typeStrong,typeWeak,img);
        res.status(200).json(pokemon);

    } catch (error) {
        res.status(400).send(error);
    } 
}