import { Request, Response } from "express";
import DigimonsService from "../services/DigimonsService";

export function getAll(_: any, res: Response) {
    const digimons = DigimonsService.getAll();
    res.status(200).json(digimons);
}

export function get(req: Request, res: Response) {
    try { 
        const id = req.params.id && +req.params.id || undefined;
        if(!id){ throw "Se requiere el ID del digimon."}
        const digimon = DigimonsService.get(id);
        res.status(200).json(digimon);
    } catch (error) {
        res.status(400).send(error);
    }
}

export function getByName(req: Request, res: Response) {
    try {
        const name = req.params.name && req.params.name || undefined;
        if(!name){ throw "Se requiere el name del digimon."}
        const digimons = DigimonsService.getByName(name);
        res.status(200).json(digimons);
    } catch (error) {
        res.status(400).send(error);
    }
}

export function getByType(req: Request, res: Response) {
    try {
        const type = req.params.type && req.params.type || undefined;
        if(!type){ throw "Se requiere el Tipo del digimon."}
        const digimons = DigimonsService.getByType(type);
        res.status(200).json(digimons);
    } catch (error) {
        res.status(400).send(error);
    }
}

export function getStrong(req: Request, res: Response) {
    try {
        const name1 = req.params.name1 && req.params.name1 || undefined;
        const name2 = req.params.name2 && req.params.name2 || undefined;
        if(!name1||!name2){ throw "Se requieren los dos Digimones."}
        const isStrong = DigimonsService.getStrong(name1,name2);
        res.status(200).json(isStrong);
    } catch (error) {
        res.status(400).send(error);
    }
}

export function getWeak(req: Request, res: Response) {
    try {
        const name1 = req.params.name1 && req.params.name1 || undefined;
        const name2 = req.params.name2 && req.params.name2 || undefined;
        if(!name1||!name2){ throw "Se requieren los dos Digimones."}
        const isStrong = DigimonsService.getWeak(name1,name2);
        res.status(200).json(isStrong);
    } catch (error) {
        res.status(400).send(error);
    } 
}

export function newDigimon(req: Request, res: Response) {
    try {
        const name = req.params.name && req.params.name || undefined;
        const id = parseInt(req.params.id && req.params.id) || undefined;
        const typeName = req.params.typeName && req.params.typeName || undefined;
        const typeStrong = req.params.typeStrong && req.params.typeStrong || undefined;
        const typeWeak = req.params.typeWeak && req.params.typeWeak || undefined;
        const img = req.params.img && req.params.img || undefined;

        if(!name||!id||!typeName||!typeStrong||!typeWeak||!img){ throw "Se requieren los dos Digimones."}
        const digimon = DigimonsService.newDigimon(id,name,typeName,typeStrong,typeWeak,img);
        res.status(200).json(digimon);

    } catch (error) {
        res.status(400).send(error);
    } 
}