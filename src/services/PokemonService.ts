import e from "express";
import { PokemonI } from "../interfaces/PokemonInterfaces";
import { MonsterTypeI } from "../interfaces/MonsterTypeI";
const db = require('../db/Pokemons.json');

module PokemonService { 
    export function getAll(): Array<PokemonI> {
        const pokemon: Array<PokemonI> = db;
        return pokemon
    }
        
    export function get(id: number): PokemonI {
        const pokemons: Array<PokemonI> = db;
        const pokemon: Array<PokemonI> = pokemons.filter(e => e.id === id);
        if (pokemon.length < 1) {
            throw "No se encontró el pokemon"
        }
        return pokemon[0];
    }

    export function getByName(name: string): Array<PokemonI> {
        const pokemons: Array<PokemonI> = db;
        const matches: Array<PokemonI> = pokemons.filter(function(el) {
            return el.name.toLowerCase().indexOf(name.toLowerCase()) > -1;
        })
        if (matches.length < 1) {
            throw "No se encontró el pokemon"
        }
        return matches;
    }
    
    export function getByType(type: string): Array<PokemonI> {
        const pokemons: Array<PokemonI> = db;
        let matches: Array<PokemonI> = [];
        pokemons.forEach(pokemon => {
            const found = pokemon.type.filter(e => e.name === type);
            if (found.length>0) {
                matches.push(pokemon);
            }
        })
         
        if (matches.length < 1) {
            throw "No se encontró el tipo"
        } 
        return matches;
    }

    export function newPokemon(id:number,namePokemon:string,typeName:string,typeStrong:string,typeWeak:string,imagen:string): PokemonI {
        
        let typePokemon: MonsterTypeI={
            name: typeName,
            strongAgainst: typeStrong,
            weakAgainst: typeWeak
        }

        let typeArray: Array<MonsterTypeI> =[];
        typeArray[0]=typePokemon;
        
        let newPokemon:PokemonI ={
            id: id,
            name: namePokemon,
            type: typeArray,
            img: imagen
        }
        db.push(newPokemon);
        return newPokemon;
    }

    export function getStrong(name: string,name2:string): string {
        // console.log(name);
        // console.log(name2);

        const pokemons: Array<PokemonI> = db;
        const pokemons2: Array<PokemonI> = db;
        let matches: Array<PokemonI> = [];

        let pokemon1: Array<PokemonI> = [];
        let pokemon2: Array<PokemonI> = [];

        pokemon1 = pokemons.filter(e => e.name.toLowerCase() === name.toLowerCase());

        if (pokemon1.length < 1) {
            throw "No se encontró el primer pokemon"
        }

        pokemon2 = pokemons2.filter(e => e.name.toLowerCase() === name2.toLowerCase());
        const type1 = pokemon1[0].type.filter(e => e.name);

        pokemons.forEach(element => {
            const strong = element.type.filter(e => e.weakAgainst === type1[0].name);
            // console.log(strong);
            if (strong.length>0) {
                matches.push(element);
            }
        })

        var isStrong:boolean;
        isStrong=false;

        // for(var i=0;matches.length;i++){
        //     console.log(matches[i].name);
        //     if(matches[i].name==name2){
        //         isStrong=true;
        //     }
        // }

        // console.log(matches.length);
        
        // if(matches[0].name==name2){
        //     isStrong=true;
        // }
         

        // console.log(isStrong);

        if (matches.length < 1) {
            throw "No se encontró el pokemon"
        }

        matches.forEach(element => {
            // console.log(element.name);
            if(element.name==name2){
                isStrong=true;
            }
        });

        if (pokemon2.length < 1) {
            throw "No se encontró el segundo pokemon"
        }

        
        if(isStrong){
            return "El Pokemon "+name+" es fuerte contra "+name2;
        }
        return "El Pokemon "+name+" no es fuerte contra "+name2;
    }

    export function getWeak(name: string,name2:string): string {

        const pokemons: Array<PokemonI> = db;
        const pokemons2: Array<PokemonI> = db;
        let matches: Array<PokemonI> = [];

        let pokemon1: Array<PokemonI> = [];
        let pokemon2: Array<PokemonI> = [];


        pokemon1 = pokemons.filter(e => e.name.toLowerCase() === name.toLowerCase());
        if (pokemon1.length < 1) {
            throw "No se encontró el primer pokemon"
        }

        pokemon2 = pokemons2.filter(e => e.name.toLowerCase() === name2.toLowerCase());
        if (pokemon2.length < 1) {
            throw "No se encontró el segundo pokemon"
        }

        const type1 = pokemon1[0].type.filter(e => e.name);

        pokemons.forEach(pokemon => {
            const strong = pokemon.type.filter(e => e.strongAgainst === type1[0].name);
            // console.log(strong);
            if (strong.length>0) {
                matches.push(pokemon);
            }
        })

        var isStrong:boolean;
        isStrong=false;


        if (matches.length < 1) {
            throw "No se encontró el pokemon"
        }

        matches.forEach(element => {
            if(element.name==name2){
                isStrong=true;
            }
        });

        
        if(isStrong){
            return "El Pokemon "+name+" es débil contra "+name2;
        }
        return "El Pokemon "+name+" no es débil contra "+name2;
    }
}

export default PokemonService;
