import e from "express";
import { DigimonI } from "../interfaces/DigimonInterfaces";
import { MonsterTypeI } from "../interfaces/MonsterTypeI";
const db = require('../db/Digimons.json');

module DigimonsService { 
    export function getAll(): Array<DigimonI> {
        const digimons: Array<DigimonI> = db;
        return digimons
    }
    
    export function get(id: number): DigimonI {
        const digimons: Array<DigimonI> = db;
        const digimon: Array<DigimonI> = digimons.filter(e => e.id === id);
        if (digimon.length < 1) {
            throw "No se encontró el digimon"
        }
        return digimon[0];
    }

    export function getByName(name: string): Array<DigimonI> {
        const digimons: Array<DigimonI> = db;
        const matches: Array<DigimonI> = digimons.filter(function(el) {
            return el.name.toLowerCase().indexOf(name.toLowerCase()) > -1;
        })
        if (matches.length < 1) {
            throw "No se encontró el digimon"
        }
        return matches;
    }
    
    export function getByType(type: string): Array<DigimonI> {
        const digimons: Array<DigimonI> = db;
        let matches: Array<DigimonI> = [];
        digimons.forEach(digimon => {
            const found = digimon.type.filter(e => e.name === type);
            if (found.length>0) {
                matches.push(digimon);
            }
        })
         
        if (matches.length < 1) {
            throw "No se encontró el tipo"
        } 
        return matches;
    }

    export function newDigimon(id:number,nameDigimon:string,typeName:string,typeStrong:string,typeWeak:string,imagen:string): DigimonI {
        
        let typeDigimon: MonsterTypeI={
            name: typeName,
            strongAgainst: typeStrong,
            weakAgainst: typeWeak
        }

        let typeArray: Array<MonsterTypeI> =[];
        typeArray[0]=typeDigimon;
        
        let newDigimon:DigimonI ={
            id: id,
            name: nameDigimon,
            type: typeArray,
            img: imagen
        }
        db.push(newDigimon);
        return newDigimon;
    }

    export function getStrong(name: string,name2:string): string {
        // console.log(name);
        // console.log(name2);

        const digimons: Array<DigimonI> = db;
        const digimons2: Array<DigimonI> = db;
        let matches: Array<DigimonI> = [];

        let digimon1: Array<DigimonI> = [];
        let digimon2: Array<DigimonI> = [];

        digimon1 = digimons.filter(e => e.name.toLowerCase() === name.toLowerCase());

        if (digimon1.length < 1) {
            throw "No se encontró el primer digimon"
        }

        digimon2 = digimons2.filter(e => e.name.toLowerCase() === name2.toLowerCase());
        const type1 = digimon1[0].type.filter(e => e.name);

        digimons.forEach(digimon => {
            const strong = digimon.type.filter(e => e.weakAgainst === type1[0].name);
            // console.log(strong);
            if (strong.length>0) {
                matches.push(digimon);
            }
        })

        var isStrong:boolean;
        isStrong=false;

        // for(var i=0;matches.length;i++){
        //     console.log(matches[i].name);
        //     if(matches[i].name==name2){
        //         isStrong=true;
        //     }
        // }

        // console.log(matches.length);
        
        // if(matches[0].name==name2){
        //     isStrong=true;
        // }
         

        // console.log(isStrong);

        if (matches.length < 1) {
            throw "No se encontró el digimon"
        }

        matches.forEach(element => {
            // console.log(element.name);
            if(element.name==name2){
                isStrong=true;
            }
        });

        if (digimon2.length < 1) {
            throw "No se encontró el segundo digimon"
        }

        
        if(isStrong){
            return "El Digimon "+name+" es fuerte contra "+name2;
        }
        return "El Digimon "+name+" no es fuerte contra "+name2;
    }

    export function getWeak(name: string,name2:string): string {

        const digimons: Array<DigimonI> = db;
        const digimons2: Array<DigimonI> = db;
        let matches: Array<DigimonI> = [];

        let digimon1: Array<DigimonI> = [];
        let digimon2: Array<DigimonI> = [];

        digimon1 = digimons.filter(e => e.name.toLowerCase() === name.toLowerCase());
        if (digimon1.length < 1) {
            throw "No se encontró el primer digimon"
        }

        digimon2 = digimons2.filter(e => e.name.toLowerCase() === name2.toLowerCase());
        if (digimon2.length < 1) {
            throw "No se encontró el segundo digimon"
        }

        const type1 = digimon1[0].type.filter(e => e.name);

        digimons.forEach(digimon => {
            const strong = digimon.type.filter(e => e.strongAgainst === type1[0].name);
            // console.log(strong);
            if (strong.length>0) {
                matches.push(digimon);
            }
        })

        var isStrong:boolean;
        isStrong=false;


        if (matches.length < 1) {
            throw "No se encontró el digimon"
        }

        matches.forEach(element => {
            if(element.name==name2){
                isStrong=true;
            }
        });

        
        if(isStrong){
            return "El Digimon "+name+" es débil contra "+name2;
        }
        return "El Digimon "+name+" no es débil contra "+name2;
    }
}

export default DigimonsService;
